<div id="staticMap" style="text-align:center">
    <img src="https://maps.googleapis.com/maps/api/staticmap?center={$address}$zoom{$zoom}&size={$size}&maptype=roadmap&markers=color:{$color}%7Clabel:{$title}%7C{$address}&key={$api_key}"/>
</div>