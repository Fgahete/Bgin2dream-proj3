<?php

if (!defined('_PS_VERSION_')) {
    exit;
}

class fgmodactu extends Module {

    public function __construct() {
        $this->name = 'fgmodactu';
        $this->tab = 'module_perso';
        $this->version = '1.0';
        $this->author = 'Catchu';

        parent::__construct();

        $this->displayName = $this->l('Module d\'actualités');
        $this->description = $this->l('Ce module vous permet de faire apparaître des actualités et informations sur votre site.');

        parent::__construct();
    }

    public function install() {
        if (!parent::install()) {
            return false;
        }
        if (!$this->registerHook('displayFooterBefore') || !$this->registerHook('displayHome')) {
            return false;
        }
        Configuration::updateValue('FGMODACTU_TITLE', '');
        Configuration::updateValue('FGMODACTU_DESC', '');
        Configuration::updateValue('FGMODACTU_DATEDEB', '');
        Configuration::updateValue('FGMODACTU_DATEFIN', '');
        Configuration::updateValue('FGMODACTU_CONTENU', '');
        return true;
    }

    public function uninstall() {
        if (!parent::uninstall()) {
            return false;
        }
        Configuration::deleteByName('FGMODACTU_TITLE', '');
        Configuration::deleteByName('FGMODACTU_DESC', '');
        Configuration::deleteByName('FGMODACTU_DATEDEB', '');
        Configuration::deleteByName('FGMODACTU_DATEFIN', '');
        Configuration::deleteByName('FGMODACTU_CONTENU', '');
        return true;
    }

    public function getContent() {
        $html = '';
        $html .= $this->processConfiguration();
        $html .= $this->displayForm();
        return $html;
    }

    public function getConfigFieldsValues() {
        return array(
            'FGMODACTU_TITLE' => Tools::getValue('FGMODACTU_TITLE', Configuration::get('FGMODACTU_TITLE')),
            'FGMODACTU_DESC' => Tools::getValue('FGMODACTU_DESCS', Configuration::get('FGMODACTU_DESC')),
            'FGMODACTU_DATEDEB' => Tools::getValue('FGMODACTU_DATEDEB', Configuration::get('FGMODACTU_DATEDEB')),
            'FGMODACTU_DATEFIN' => Tools::getValue('FGMODACTU_DATEFIN', Configuration::get('FGMODACTU_DATEFIN')),
            'FGMODACTU_CONTENU' => Tools::getValue('FGMODACTU_CONTENU', Configuration::get('FGMODACTU_CONTENU')),
        );
    }

    public function displayForm() {

        $fields_form = array(
            'form' => array(
                'legend' => array(
                    'title' => $this->l('Settings'),
                    'icon' => 'icon-cogs'),
                'input' => array(
                    array(
                        'type' => 'text',
                        'label' => $this->l('Title'),
                        'name' => 'FGMODACTU_TITLE',
                    ),
                    array(
                        'type' => 'text',
                        'label' => $this->l('Description'),
                        'name' => 'FGMODACTU_DESC',
                    ),
                    array(
                        'type' => 'date',
                        'label' => $this->l('Start Date'),
                        'name' => 'FGMODACTU_DATEDEB',
                    ),
                    array(
                        'type' => 'date',
                        'label' => $this->l('End Date'),
                        'name' => 'FGMODACTU_DATEFIN',
                    ),
                    array(
                        'type' => 'textarea',
                        'label' => $this->l('Contain'),
                        'name' => 'FGMODACTU_CONTENU',
                    )
                ),
                'submit' => array(
                    'title' => $this->l('Save'),
                )
            ),
        );


        $helper = new HelperForm();
        $helper->show_toolbar = false;
        $helper->table = $this->table;
        $lang = new Language((int) Configuration::get('PS_LANG_DEFAULT'));
        $helper->default_form_language = $lang->id;
        $this->fields_form = array();

        $helper->identifier = $this->identifier;
        $helper->submit_action = 'submit_fg_modactu_form';
        $helper->currentIndex = $this->context->link->getAdminLink('AdminModules', false) . '&configure=' . $this->name . '&tab_module=' . $this->tab . '&module_name=' . $this->name;
        $helper->token = Tools::getAdminTokenLite('AdminModules');
        $helper->tpl_vars = array(
            'fields_value' => $this->getConfigFieldsValues(),
            'languages' => $this->context->controller->getLanguages(),
            'id_language' => $this->context->language->id
        );
        return $helper->generateForm(array($fields_form));
    }

    public function processConfiguration() {
        if (Tools::isSubmit('submit_fg_modactu_form')) {
            Configuration::updateValue('FGMODACTU_TITLE', Tools::getValue('FGMODACTU_TITLE'));
            Configuration::updateValue('FGMODACTU_DESC', Tools::getValue('FGMODACTU_DESC'));
            Configuration::updateValue('FGMODACTU_DATEDEB', Tools::getValue('FGMODACTU_DATEDEB'));
            Configuration::updateValue('FGMODACTU_DATEFIN', Tools::getValue('FGMODACTU_DATEFIN'));
            Configuration::updateValue('FGMODACTU_CONTENU', Tools::getValue('FGMODACTU_CONTENU'));
            return $this->displayConfirmation('La configuration a été mise à jour avec succès.');
        }
    }

    public function assignValuesToTemplate() {
        $title = Configuration::get('FGMODACTU_TITLE');
        $desc = Configuration::get('FGMODACTU_DESC');
        $datedeb = Configuration::get('FGMODACTU_DATEDEB');
        $datefin = Configuration::get('FGMODACTU_DATEFIN');
        $contenu = Configuration::get('FGMODACTU_CONTENU');

        $this->context->smarty->assign('title', $title);
        $this->context->smarty->assign('desc', $desc);
        $this->context->smarty->assign('datedeb', $datedeb);
        $this->context->smarty->assign('datefin', $datefin);
        $this->context->smarty->assign('contenu', $contenu);
    }

    public function hookDisplayFooterBefore($params) {
        $this->assignValuesToTemplate();
        return $this->display(__FILE__, 'fgmodactu.tpl');
        $this->context->controller->addCSS($this->_path.'views/css/cssmodule.css', 'all');
    }
}

?>