<?php

if (!defined('_PS_VERSION_')) {
    exit;
}

class fgModApiYoutube extends Module {

    public function __construct() {
        /* General and technical informations */
        $this->name = 'fgmodapiyoutube';
        $this->version = '0.0.1';
        $this->author = 'Catchu';
        $this->tab = 'front_office_features';
        /* $this->ps_versions_compliancy = array('min'=> '1.6.0', 'max' => '1.7.2'); */

        /* Public informations */
        $this->displayName = $this->l('Adding youtube into your universe.');
        $this->description = $this->l('With this module, you can add youtube into your pages.');

        /* For admin panel from 1.6 */
        $this->bootstrap = true;

        parent::__construct();
    }

    public function install() {
        if (!parent::install()) {
            return false;
        }
        if (!$this->registerHook('displayFooterBefore')) {
            //displayOverridetemplate
            return false;
        }
        /*Configuration::updateValue('FGMODAPIYOUTUBE_APIKEY', '');*/
        Configuration::updateValue('FGMODAPIYOUTUBE_ADDRESS', '');
        Configuration::updateValue('FGMODAPIYOUTUBE_TITLE', '');
        /*Configuration::updateValue('FGMODAPIYOUTUBE_HEIGHT', '');
        Configuration::updateValue('FGMODAPIYOUTUBE_WIDTH', '');*/
        return true;
    }

    public function uninstall() {
        if (!parent::uninstall()) {
            return false;
        }
       /* Configuration::updateValue('FGMODAPIYOUTUBE_APIKEY', '');*/
        Configuration::deleteByName('FGMODAPIYOUTUBE_ADDRESS', '');
        Configuration::deleteByName('FGMODAPIYOUTUBE_TITLE', '');
       /* Configuration::deleteByName('FGMODAPIYOUTUBE_HEIGHT', '');
        Configuration::deleteByName('FGMODAPIYOUTUBE_WIDTH', '');*/

        return true;
    }

    public function getContent() {
        $html = '';
        $html .= $this->processConfiguration();
        $html .= $this->displayForm();
        return $html;
    }

    public function getConfigFieldsValues() {
        return array(
           /* 'FGMODAPIYOUTUBE_APIKEY' => Tools::getValue('FGMODAPIYOUTUBE_APIKEY', Configuration::get('FGMODAPIYOUTUBE_APIKEY')),*/
            'FGMODAPIYOUTUBE_ADDRESS' => Tools::getValue('FGMODAPIYOUTUBE_ADDRESS', Configuration::get('FGMODAPIYOUTUBE_ADDRESS')),
            'FGMODAPIYOUTUBE_TITLE' => Tools::getValue('FGMODAPIYOUTUBE_TITLE', Configuration::get('FGMODAPIYOUTUBE_TITLE')),
           /* 'FGMODAPIYOUTUBE_ADDRESS' => Tools::getValue('FGMODAPIYOUTUBE_HEIGHT', Configuration::get('FGMODAPIYOUTUBE_HEIGTH')),
            'FGMODAPIYOUTUBE_ADDRESS' => Tools::getValue('FGMODAPIYOUTUBE_WIDTH', Configuration::get('FGMODAPIYOUTUBE_WIDTH')),*/
        );
    }

    public function displayForm() {
        /*$heightOptions = array(
            array(
                'id_heightOptions' => '400', // 'value' attribute of the <option> tag.
                'heightOptions' => '270 pixels'   //affiché // text content of the  <option> tag.
            ),
            array(
                'id_heightOptions' => '550',
                'heightOptions' => '500 pixels'
            )
        );

        $widthOptions = array(
            array(
                'id_widthOptions' => '480', // The value of the 'value' attribute of the <option> tag.
                'widthOptions' => '480 pixels'   //affiché // The value of the text content of the  <option> tag.
            ),
            array(
                'id_widthOptions' => '550',
                'widthOptions' => '550 pixels'
            ),
            array(
                'id_widthOptions' => '700',
                'widthOptions' => '700 pixels'
            )
        );*/

        $fields_form = array(
            'form' => array(
                'legend' => array(
                    'title' => $this->l('Settings'),
                    'icon' => 'icon-cogs'
                ),
                'input' => array(
/*                        'type' => 'text',
                        'label' => $this->l('Api key'),
                        'name' => 'FGMODAPIYOUTUBE_APIKEY',
                        'desc' => $this->l('Storing API key given by Google.'),
                    ),*/
                    array(
                        'type' => 'text',
                        'label' => $this->l('Address'),
                        'name' => 'FGMODAPIYOUTUBE_ADDRESS',
                        'desc' => $this->l('Write the ID youtube of your video.'),
                    ),
                    array(
                        'type' => 'text',
                        'label' => $this->l('Title of video'),
                        'name' => 'FGMODAPIYOUTUBE_TITLE',
                        'desc' => $this->l('Write the video\'s title.'),
                    ),/*
                    array(
                        'type' => 'select',
                        'label' => $this->l('Choose the height you want.'),
                        'name' => 'FGMODAPIYOUTUBE_HEIGHT',
                        'desc' => $this->l('Select a size.'),
                        'options' => array(
                            'query' => $heightOptions,
                            'id' => 'id_heightOptions',
                            'name' => 'heightOptions'
                        ),
                    ),
                    array(
                        'type' => 'select',
                        'label' => $this->l('Choose the width you want'),
                        'name' => 'FGMODAPIYOUTUBE_WIDTH',
                        'desc' => $this->l('Choose the width you want.'),
                        'options' => array(
                            'query' => $widthOptions,
                            'id' => 'id_widthOptions',
                            'name' => 'widthOptions'
                        ),
                    ),*/
                ),
                'submit' => array(
                    'title' => $this->l('Save'),
                )
            ),
        );


        $helper = new HelperForm();
        $helper->show_toolbar = false;
        $helper->table = $this->table;
        $lang = new Language((int) Configuration::get('PS_LANG_DEFAULT'));
        $helper->default_form_language = $lang->id;
        $this->fields_form = array();

        $helper->identifier = $this->identifier;
        $helper->submit_action = 'submit_fg_api_youtube_form';
        $helper->currentIndex = $this->context->link->getAdminLink('AdminModules', false) . '&configure=' . $this->name . '&tab_module=' . $this->tab . '&module_name=' . $this->name;
        $helper->token = Tools::getAdminTokenLite('AdminModules');
        $helper->tpl_vars = array(
            'fields_value' => $this->getConfigFieldsValues(),
            'languages' => $this->context->controller->getLanguages(),
            'id_language' => $this->context->language->id
        );
        return $helper->generateForm(array($fields_form));
    }

    public function processConfiguration() {
        if (Tools::isSubmit('submit_fg_api_youtube_form')) {
          /*  Configuration::updateValue('FGMODAPIYOUTUBE_APIKEY', Tools::getValue('FGMODAPIYOUTUBE_APIKEY'));*/
            Configuration::updateValue('FGMODAPIYOUTUBE_ADDRESS', Tools::getValue('FGMODAPIYOUTUBE_ADDRESS'));
            Configuration::updateValue('FGMODAPIYOUTUBE_TITLE', Tools::getValue('FGMODAPIYOUTUBE_TITLE'));
            /*Configuration::updateValue('FGMODAPIYOUTUBE_HEIGHT', Tools::getValue('FGMODAPIYOUTUBE_HEIGHT'));
            Configuration::updateValue('FGMODAPIYOUTUBE_WIDTH', Tools::getValue('FGMODAPIYOUTUBE_WIDTH'));*/
            return $this->displayConfirmation('La configuration a été mise à jour avec succès.');
        }
    }

    public function assignValuesToTemplate() {
       /* $api_key = Configuration::get('FGMODAPIYOUTUBE_APIKEY');*/
        $address = Configuration::get('FGMODAPIYOUTUBE_ADDRESS');
        $title = Configuration::get('FGMODAPIYOUTUBE_TITLE');
       /* $height = Configuration::get('FGMODAPIYOUTUBE_HEIGHT');
        $width = Configuration::get('FGMODAPIYOUTUBE_WIDTH');*/

  /*      $this->context->smarty->assign('api_key', $api_key);*/
        $this->context->smarty->assign('address', $address);
        $this->context->smarty->assign('title', $title);
       /* $this->context->smarty->assign('heigth', $height);
        $this->context->smarty->assign('width', $width);*/
    }

public function hookDisplayFooterBefore($params){
        $this->assignValuesToTemplate();
        return $this->display(__FILE__, 'hookdisplayfootbef.tpl');
    }
}

?>