<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{fgmodapiyoutube}prestashop>fgmodapiyoutube_b6daa94123e8b077878c2b95b9793f01'] = 'Ajouter youtube à votre univers.';
$_MODULE['<{fgmodapiyoutube}prestashop>fgmodapiyoutube_f6f12ebc45204981a6497d8a37b8d293'] = 'Avec ce module vous pouvez ajouter un lecteur youtube dans vos pages.';
$_MODULE['<{fgmodapiyoutube}prestashop>fgmodapiyoutube_62c7594948c1231bbae9f948b1535b7e'] = 'Clef d\'API';
$_MODULE['<{fgmodapiyoutube}prestashop>fgmodapiyoutube_fc0128f6bebc4cefedc3f4bc41b2a5cb'] = 'Entrez ici l\'API fourni par google';
$_MODULE['<{fgmodapiyoutube}prestashop>fgmodapiyoutube_dd7bf230fde8d4836917806aff6a6b27'] = 'Adresse (ID)';
$_MODULE['<{fgmodapiyoutube}prestashop>fgmodapiyoutube_cae9e5088ccb935898d165359271a6bc'] = 'Inscrivez ici l\'ID youtube d\'une playlist. Exemple : PLOba6OKTJnLbDvwBBEwO1EaVsiICn8Svw';
$_MODULE['<{fgmodapiyoutube}prestashop>fgmodapiyoutube_449dac6147ae924ab5a17f6f081aa1e6'] = 'Titre de la vidéo';
$_MODULE['<{fgmodapiyoutube}prestashop>fgmodapiyoutube_1435a36a506687a440d75808a8b0f19f'] = 'Ecrivez le titre de la vidéo';
$_MODULE['<{fgmodapiyoutube}prestashop>fgmodapiyoutube_c9cc8cce247e49bae79f15173ce97354'] = 'Sauvegarder';
