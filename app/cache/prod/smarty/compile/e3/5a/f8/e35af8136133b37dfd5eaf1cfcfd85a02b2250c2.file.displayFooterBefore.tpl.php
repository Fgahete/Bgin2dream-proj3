<?php /* Smarty version Smarty-3.1.19, created on 2018-04-18 11:41:36
         compiled from "modules\fgmodstaticmap\views\templates\hook\displayFooterBefore.tpl" */ ?>
<?php /*%%SmartyHeaderCode:51915ad712d08d4a46-57310330%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'e35af8136133b37dfd5eaf1cfcfd85a02b2250c2' => 
    array (
      0 => 'modules\\fgmodstaticmap\\views\\templates\\hook\\displayFooterBefore.tpl',
      1 => 1523360902,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '51915ad712d08d4a46-57310330',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'address' => 0,
    'zoom' => 0,
    'size' => 0,
    'color' => 0,
    'title' => 0,
    'api_key' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_5ad712d08dbc88_56230101',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5ad712d08dbc88_56230101')) {function content_5ad712d08dbc88_56230101($_smarty_tpl) {?><div id="staticMap" style="text-align:center">
    <img src="https://maps.googleapis.com/maps/api/staticmap?center=<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['address']->value, ENT_QUOTES, 'UTF-8');?>
$zoom<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['zoom']->value, ENT_QUOTES, 'UTF-8');?>
&size=<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['size']->value, ENT_QUOTES, 'UTF-8');?>
&maptype=roadmap&markers=color:<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['color']->value, ENT_QUOTES, 'UTF-8');?>
%7Clabel:<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['title']->value, ENT_QUOTES, 'UTF-8');?>
%7C<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['address']->value, ENT_QUOTES, 'UTF-8');?>
&key=<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['api_key']->value, ENT_QUOTES, 'UTF-8');?>
"/>
</div><?php }} ?>
