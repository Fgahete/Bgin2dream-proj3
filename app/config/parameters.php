<?php return array (
  'parameters' => 
  array (
    'database_host' => '127.0.0.1',
    'database_port' => '',
    'database_name' => 'proj3',
    'database_user' => 'root',
    'database_password' => '',
    'database_prefix' => 'pj3_',
    'database_engine' => 'InnoDB',
    'mailer_transport' => 'smtp',
    'mailer_host' => '127.0.0.1',
    'mailer_user' => NULL,
    'mailer_password' => NULL,
    'secret' => '4HK7uDV29EH7thQWbGGVtAgj1vrVdSZenCxg0iKc3L6divZt4Tw16t8m',
    'ps_caching' => 'CacheMemcache',
    'ps_cache_enable' => false,
    'ps_creation_date' => '2018-04-05',
    'locale' => 'fr-FR',
    'cookie_key' => 'GutuRWYqTokhhAiezr3RmH5BiR23BLzSGm4GaGASgqWj1YGA70fovECk',
    'cookie_iv' => 'YnCFhVC5',
    'new_cookie_key' => 'def0000097d4d276796672257f2fe55bc0788b6ea0c9881963dce2ec08993802b54441023d917d35d21e7f6b6c3dc078395c155292c3665b70fbe31a5d9e44c2bd429bfd',
  ),
);